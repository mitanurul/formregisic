<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>PHP</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		/* Show it is fixed to the top */
		body {
		  min-height: 75rem;
		  padding-top: 5.5rem;
		}
	</style>
</head>

<?php include "config/koneksi.php"; 
	$guests = mysqli_query($con, "SELECT * FROM guest");
?>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <a class="navbar-brand" href="#">BRAND</a>
	  </div>
	</nav>

	<div class="container"> 
	    <div class="row" >

			<div class="col-md-4">
				<div class="card">
			  		<div class="card-header">Input</div>
		  			<form method="post" action="save_guest.php" role="form">
				  	<div class="card-body">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="Name*" required>
							<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
						</div>
						<div class="form-group">
							<label for="contact">Contact</label>
							<input type="text" class="form-control" id="contact" name="contact" placeholder="08xxx">
						</div>
			            <div class="form-group">
			            	<label for="addr">Address</label>
			            	<textarea class="form-control" type="textarea" id="addr" name="address" placeholder="Alamat" maxlength="140" rows="7"></textarea>
			                <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>          
			            </div> 
			        </div>
				  	<div class="card-footer">
				  		<a href="javascript:location.reload(true)"  class="btn btn-light">Cancel</a>
				  		<button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Submit</button>
				  	</div>

			  		</form>
				</div>
				
			</div>

				
			<div class="col-md-8">
				<div class="card">
				  <div class="card-header">
				  	List Users
				  </div>
				  <div class="card-body">
							
					 <table class="table table-striped">
					    <thead>
					      <tr>
					        <th>Nama</th>
				            <th>No HP</th>
				            <th>Alamat</th>
				            <th>Action</th>
					      </tr>
					    </thead>
					    <tbody>
				        	<?php while ($g = mysqli_fetch_array($guests)){ ?>
					            <tr>
					                <td width="10%"><?php echo $g['name'] ?></td>
					                <td width="10%"><?php echo $g['contact'] ?></td>
					                <td><?php echo $g['address'] ?></td>
					               
								   <td><button type="button" class="btn btn-secondary btn-sm" style="margin-right: 4px" data-toggle="modal" data-target="#edit-<?php echo $g['id'] ?>"><i class="glyphicon glyphicon-pencil"></i> Edit</button>

					                	<a href="delete_guest.php?user=<?php echo $g['id'] ?>"  class="btn btn-danger btn-sm" onclick="return confirm('Apakah user ini akan di hapus ?');"><i class="glyphicon glyphicon-trash"></i> Del</a>
					         
					            	</td>
					            </tr>
								<!-- Trigger the modal with a button -->
								<!-- Modal -->
								<div id="edit-<?php echo $g['id']; ?>" class="modal" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <!-- Modal Header -->
								      <div class="modal-header">
								        <h4 class="modal-title">Edit</h4>
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								      </div>

								      <form  method="post" action="save_edit_guest.php?user=<?php echo $g['id'] ?>" role="form">
									      <div class="modal-body">
									      	<div class="row">
									      		<div class="col-md-12">
								    				<div class="form-group">
								    					<label>Name</label>
														<input type="text" class="form-control" id="name" name="name" placeholder="Name*" value="<?php echo $g['name'] ?>" required>
														<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
													</div>
													<div class="form-group">
								    					<label> Contact </label>
														<input type="text" class="form-control" id="mobile" name="contact" placeholder="08xxx" value="<?php echo $g['contact'] ?>">
													</div>
								                    <div class="form-group">
								    					<label> Address </label>
								                    	<textarea class="form-control" type="textarea" id="message_edit" name="address" placeholder="Alamat" maxlength="140" rows="7"> <?php echo $g['address'] ?> </textarea>        
								                    </div>
										    	</div>
										    </div>
									      </div>
									      <div class="modal-footer">
									        <button type="submit" name="submit" class="btn btn-primary pull-right">Save</button>
									      </div>
								      </form>
								    </div>
								  </div>
								</div>
				        	<?php } ?>
					    </tbody>
					  </table>
				  </div> 
				</div>
			</div>

		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#characterLeft').text('140 characters left');
		    $('#addr').keydown(function () {
		        var max = 140;
		        var len = $(this).val().length;
		        if (len >= max) {
		            $('#characterLeft').text('You have reached the limit');
		            $('#characterLeft').addClass('red');
		            $('#btnSubmit').addClass('disabled');            
		        } 
		        else {
		            var ch = max - len;
		            $('#characterLeft').text(ch + ' characters left');
		            $('#btnSubmit').removeClass('disabled');
		            $('#characterLeft').removeClass('red');            
		        }
		    });  

		} );
	</script>

</body>
</html>

